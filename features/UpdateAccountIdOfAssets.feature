Feature: Update Account Id Of Assets
  Updates the account id of the provided assets

  Background:
    Given an updateAccountIdOfAssetsReq consists of:
      | attribute | validation | type             |
      | assetIds  | required   | array of strings |
      | accountId | required   | string           |

  Scenario: Success
    Given I provide an accountId of an existing account in the account-service
    And I provide assetIds of existing assets in the asset-service
    And provide an accessToken identifying me as a partner rep
    When I executeUpdateAccountIdOfAssets
    Then the accountId of each listed asset is updated to accountId