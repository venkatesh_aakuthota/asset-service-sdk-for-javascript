Feature: Add Asset
  Adds an asset

  Background:
    Given an addAssetReq has attributes:
      | attribute     | validation | type   |
      | accountId     | required   | string |
      | productLineId | required   | number |
      | serialNumber  | required   | string |
      | description   | optional   | string |

  Scenario: Success
    Given I provide a valid addAssetReq
    And provide an accessToken identifying me as a partner rep
    When I execute addAsset
    Then the asset is added to the asset-service
    And the id is returned
