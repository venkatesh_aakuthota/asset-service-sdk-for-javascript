## Description
Precor Connect asset service SDK for javascript.

## Features

##### Add Asset
* [documentation](features/AddAsset.feature)

##### Get Assets With Ids
* [documentation](features/GetAssetsWithIds.feature)

##### List Assets With Account Id 
* [documentation](features/ListAssetsWithAccountId.feature)

##### Search For Asset With Serial Number
* [documentation](features/SearchForAssetWithSerialNumber.feature)

##### Update Account Id Of Assets
* [documentation](features/UpdateAccountIdOfAssets.feature)

## Setup

**install via jspm**  
```shell
jspm install asset-service-sdk=bitbucket:precorconnect/asset-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import AssetServiceSdk,{AssetServiceSdkConfig} from 'asset-service-sdk'

const assetServiceSdkConfig = 
    new AssetServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const assetServiceSdk = 
    new AssetServiceSdk(
        assetServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```