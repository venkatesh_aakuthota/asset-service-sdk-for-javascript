import AssetSynopsisView from '../../src/assetSynopsisView';
import dummy from '../dummy';

/*
 test methods
 */
describe('AssetSynopsisView class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AssetSynopsisView(
                        null,
                        dummy.productLineId,
                        dummy.assetSerialNumber,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.assetId;

            /*
             act
             */
            const objectUnderTest =
                new AssetSynopsisView(
                    expectedId,
                    dummy.productLineId,
                    dummy.assetSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);
        });
        it('throws if productLineId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AssetSynopsisView(
                        dummy.assetId,
                        null,
                        dummy.assetSerialNumber,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'productLineId required');
        });
        it('sets productLineId', () => {
            /*
             arrange
             */
            const expectedProductLineId = dummy.productLineId;

            /*
             act
             */
            const objectUnderTest =
                new AssetSynopsisView(
                    dummy.assetId,
                    expectedProductLineId,
                    dummy.assetSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualProductLineId = objectUnderTest.productLineId;
            expect(actualProductLineId).toEqual(expectedProductLineId);
        });
        it('throws if serialNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AssetSynopsisView(
                        dummy.assetId,
                        dummy.productLineId,
                        null,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'serialNumber required');
        });
        it('sets serialNumber', () => {
            /*
             arrange
             */
            const expectedSerialNumber = dummy.assetSerialNumber;

            /*
             act
             */
            const objectUnderTest =
                new AssetSynopsisView(
                    dummy.assetId,
                    dummy.productLineId,
                    expectedSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualSerialNumber = objectUnderTest.serialNumber;
            expect(actualSerialNumber).toEqual(expectedSerialNumber);
        });
        it('does not throw if description is null', () => {
            /*
             arrange
             */
            new AssetSynopsisView(
                dummy.assetId,
                dummy.productLineId,
                dummy.assetSerialNumber,
                null
            );
        });
        it('sets description', () => {
            /*
             arrange
             */
            const expectedDescription = dummy.assetDescription;

            /*
             act
             */
            const objectUnderTest =
                new AssetSynopsisView(
                    dummy.assetId,
                    dummy.productLineId,
                    dummy.assetSerialNumber,
                    expectedDescription
                );

            /*
             assert
             */
            const actualDescription = objectUnderTest.description;
            expect(actualDescription).toEqual(expectedDescription);
        });
    });
});
