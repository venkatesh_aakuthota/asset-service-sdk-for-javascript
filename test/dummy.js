import {AddAssetReq} from '../src/index';
import {PostalAddress} from 'postal-object-model';


/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
const dummy = {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    userId: 'fake-email@test.com',
    sapVendorNumber: '0000000000',
    url: 'https://test-url.com',
    //partnerRepId: 1,
    productLineId: 1,
    assetId: 'assetId',
    assetSerialNumber: 'assetSerialNumber',
    accountId: '000000000000000000',
    accountName: 'accountName',
    assetDescription: 'assetDescription',
    postalAddress: new PostalAddress(
        'street',
        'city',
        'WA',
        '98004',
        'US'
    )
};

dummy.addAssetReq =
    new AddAssetReq(
        dummy.accountId,
        dummy.productLineId,
        dummy.assetSerialNumber,
        dummy.assetDescription
    );

export default dummy;
