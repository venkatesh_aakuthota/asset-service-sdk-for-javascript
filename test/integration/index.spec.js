import AssetServiceSdk,{AddAssetReq,AssetServiceSdkConfig} from '../../src/index';
import UpdateAccountIdOfAssetsReq from '../../src/updateAccountIdOfAssetsReq';
import AssetSynopsisView from '../../src/assetSynopsisView';
import AccountServiceSdk,{AddCommercialAccountReq,AccountServiceSdkConfig} from 'account-service-sdk';
import config from './config';
import factory from './factory';
import dummy from '../dummy';
import jwt from 'jwt-simple';

/*
 test methods
 */
describe('Index module', () => {

    describe('default export', () => {
        it('should be AssetServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new AssetServiceSdk(config.assetServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(AssetServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addAsset method', () => {
            it('should return assetId', (done) => {

                /*
                 arrange
                 */

                const objectUnderTest =
                    new AssetServiceSdk(config.assetServiceSdkConfig);

                /*
                 act
                 */
                const assetIdPromise =
                    objectUnderTest.addAsset(
                        dummy.addAssetReq,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */

                assetIdPromise
                    .then((assetId) => {
                        expect(assetId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('getAssetsWithIds method', () => {
            it('should return expected AssetSynopsisView', (done) => {
                /*
                 arrange
                 */
                let expectedAssetSynopsisViews = [];

                const objectUnderTest =
                    new AssetServiceSdk(config.assetServiceSdkConfig);

                // seed a new asset so we can test the retrieval of it
                const addAssetRequest =
                    dummy.addAssetReq;

                const seededAssetIdPromise =
                    objectUnderTest
                        .addAsset(
                            addAssetRequest,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(
                            seededAssetId => {

                                // construct expected account contact view
                                expectedAssetSynopsisViews.push(
                                    new AssetSynopsisView(
                                        seededAssetId,
                                        addAssetRequest.productLineId,
                                        addAssetRequest.serialNumber,
                                        addAssetRequest.description
                                    )
                                );

                                return seededAssetId;
                            }
                        );

                /*
                 act
                 */
                const actualAssetSynopsisViewsPromise =
                    seededAssetIdPromise
                        .then(seededAssetId=> {

                                // get asset
                                return objectUnderTest.getAssetsWithIds(
                                    [seededAssetId],
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                )

                            }
                        );


                /*
                 assert
                 */
                actualAssetSynopsisViewsPromise
                    .then((actualAssetSynopsisViews) => {
                        expect(actualAssetSynopsisViews).toEqual(expectedAssetSynopsisViews);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('listAssetsWithAccountId', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new AssetServiceSdk(config.assetServiceSdkConfig);

                const accountId = dummy.accountId;

                // seed asset with account id
                objectUnderTest
                    .addAsset(
                        new AddAssetReq(
                            accountId,
                            dummy.productLineId,
                            dummy.assetSerialNumber,
                            dummy.assetDescription
                        ),
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 act
                 */
                const assetSynopsesPromise =
                    objectUnderTest
                        .listAssetsWithAccountId(
                            accountId,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        );

                /*
                 assert
                 */
                assetSynopsesPromise
                    .then((assetSynopses) => {
                        expect(assetSynopses.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('searchForAssetWithSerialNumber method', () => {
            it('should return expected AssetSynopsisView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new AssetServiceSdk(config.assetServiceSdkConfig);

                let expectedAssetSynopsisView =
                    config.existingSalesforceAsset;

                /*
                 act
                 */
                const actualAssetSynopsisViewPromise =
                    objectUnderTest.searchForAssetWithSerialNumber(
                        config.existingSalesforceAsset.serialNumber,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                actualAssetSynopsisViewPromise
                    .then(actualAssetSynopsisView => {
                        expect(actualAssetSynopsisView).toEqual(expectedAssetSynopsisView);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('updateAccountIdOfAssets method', () => {
            it('should update the accountId of each listed asset', done => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new AssetServiceSdk(
                        config.assetServiceSdkConfig
                    );

                const accountServiceSdk =
                    new AccountServiceSdk(
                        config.accountServiceSdkConfig
                    );

                const seededAssetIds = [];
                let seededAccountId;

                const setupPromise =
                    Promise
                        .all(
                            [
                                // seed asset
                                objectUnderTest
                                    .addAsset(
                                        dummy.addAssetReq,
                                        factory.constructValidPartnerRepOAuth2AccessToken()
                                    )
                                    .then(assetId => {
                                            seededAssetIds.push(assetId);
                                        }
                                    ),
                                // seed account
                                accountServiceSdk
                                    .addCommercialAccount(
                                        factory.constructValidAddCommercialAccountRequest(),
                                        factory.constructValidAppAccessToken()
                                    )
                                    .then(accountId => {
                                            seededAccountId = accountId;
                                        }
                                    )
                            ]
                        );


                /*
                 act
                 */
                const actPromise =
                    setupPromise.then(() =>
                        objectUnderTest
                            .updateAccountIdOfAssets(
                                new UpdateAccountIdOfAssetsReq(
                                    seededAccountId,
                                    seededAssetIds
                                ),
                                factory.constructValidPartnerRepOAuth2AccessToken()
                            )
                    );


                /*
                 assert
                 */
                actPromise
                    .then(() =>
                        objectUnderTest
                            .listAssetsWithAccountId(
                                seededAccountId,
                                factory.constructValidPartnerRepOAuth2AccessToken(
                                    seededAccountId
                                )
                            )
                    )
                    .then(assetsWithAccountId => {

                            expect(
                                Array.from(
                                    assetsWithAccountId,
                                    asset => asset.id
                                )
                            )
                                .toEqual(seededAssetIds);

                            done();

                        }
                    )
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 100000);
        });

    });
});