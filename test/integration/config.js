import AssetServiceSdkConfig from '../../src/assetServiceSdkConfig';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import AssetSynopsisView from '../../src/assetSynopsisView'

export default {
    assetServiceSdkConfig: new AssetServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    accountServiceSdkConfig: new AccountServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    existingCustomerSegmentId: 1,
    existingSalesforceAsset: new AssetSynopsisView(
        "02iK0000006rooAIAQ",
        1,
        "1444430739310",
        null
    )
}
;