export default class AssetSynopsisView {

    /*
     fields
     */
    _id:string;

    _productLineId:number;

    _serialNumber:string;

    _description:string;

    /**
     *
     * @param {string} id
     * @param {number} productLineId
     * @param {string} serialNumber
     * @param {string|null} description
     */
    constructor(id:string,
                productLineId:number,
                serialNumber:string,
                description:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!productLineId) {
            throw new TypeError('productLineId required');
        }
        this._productLineId = productLineId;

        if (!serialNumber) {
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        this._description = description;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {number}
     */
    get productLineId():number {
        return this._productLineId;
    }

    /**
     * @returns {string}
     */
    get serialNumber():string {
        return this._serialNumber;
    }

    /**
     * @returns {string}
     */
    get description():string {
        return this._description;
    }
}