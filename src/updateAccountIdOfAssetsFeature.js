import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import UpdateAccountIdOfAssetsReq from './updateAccountIdOfAssetsReq';


@inject(AssetServiceSdkConfig, HttpClient)
class UpdateAccountIdOfAssetsFeature {


    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets a transient url where a partner reps bank info can be updated
     * @param {UpdateAccountIdOfAssetsReq} request
     * @param accessToken
     * @returns {Promise}
     */
    execute(request:UpdateAccountIdOfAssetsReq,
            accessToken:string):Promise {

        return this._httpClient
            .createRequest(`assets/${request.assetIds.join(',')}/account-id`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request.accountId)
            .send();
    }
}

export default UpdateAccountIdOfAssetsFeature;