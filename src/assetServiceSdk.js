import AssetServiceSdkConfig from './assetServiceSdkConfig';
import DiContainer from './diContainer';
import AddAssetReq from './addAssetReq';
import AddAssetFeature from './addAssetFeature';
import AssetSynopsisView from './assetSynopsisView';
import GetAssetsWithIdsFeature from './getAssetsWithIdsFeature';
import ListAssetsWithAccountIdFeature from './listAssetsWithAccountIdFeature';
import SearchForAssetWithSerialNumberFeature from './searchForAssetWithSerialNumberFeature';
import UpdateAccountIdOfAssetsReq from './updateAccountIdOfAssetsReq';
import UpdateAccountIdOfAssetsFeature from './updateAccountIdOfAssetsFeature';

/**
 * @class {AssetServiceSdk}
 */
export default class AssetServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {AssetServiceSdkConfig} config
     */
    constructor(config:AssetServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addAsset(request:AddAssetReq,
             accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddAssetFeature)
            .execute(
                request,
                accessToken
            );

    }

    getAssetsWithIds(ids:Array<string>,
                     accessToken:string):Promise<AssetSynopsisView> {

        return this
            ._diContainer
            .get(GetAssetsWithIdsFeature)
            .execute(
                ids,
                accessToken
            );
    }

    listAssetsWithAccountId(accountId:string,
                            accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListAssetsWithAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

    searchForAssetWithSerialNumber(serialNumber:string,
                                   accessToken:string):Promise<AssetSynopsisView> {

        return this
            ._diContainer
            .get(SearchForAssetWithSerialNumberFeature)
            .execute(
                serialNumber,
                accessToken
            );

    }

    updateAccountIdOfAssets(request:UpdateAccountIdOfAssetsReq,
                            accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdateAccountIdOfAssetsFeature)
            .execute(
                request,
                accessToken
            );

    }

}
