import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AssetSynopsisView from './assetSynopsisView';

@inject(AssetServiceSdkConfig, HttpClient)
class ListAssetsWithAccountIdFeature {

    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all assets with the provided account id
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<AssetSynopsisView[]>}
     */
    execute(accountId:string,
            accessToken:string):Promise<Array> {


        return this._httpClient
            .createRequest(`assets`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                accountId: accountId
            })
            .send()
            .then((response) =>
                Array.from(
                    response.content,
                    contentItem =>
                        new AssetSynopsisView(
                            contentItem.id,
                            contentItem.productLineId,
                            contentItem.serialNumber,
                            contentItem.description
                        )
                )
            );
    }
}

export default ListAssetsWithAccountIdFeature;