import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AssetSynopsisView from './assetSynopsisView';

@inject(AssetServiceSdkConfig, HttpClient)
class GetAssetsWithIdsFeature {

    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets the assets with the provided ids
     * @param {string[]} ids
     * @param {string} accessToken
     * @returns {Promise.<AssetSynopsisView[]>}
     */
    execute(ids:Array<string>,
            accessToken:string):Promise<Array> {

        return this._httpClient
            .createRequest(`assets/${ids.join(',')}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new AssetSynopsisView(
                            contentItem.id,
                            contentItem.productLineId,
                            contentItem.serialNumber,
                            contentItem.description
                        )
                )
            );
    }
}

export default GetAssetsWithIdsFeature;