import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AssetSynopsisView from './assetSynopsisView';

@inject(AssetServiceSdkConfig, HttpClient)
class SearchForAssetWithSerialNumberFeature {

    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Searches for an asset with the provided serial number
     * @param {string} serialNumber
     * @param {string} accessToken
     * @returns {Promise.<AssetSynopsisView|null>}
     */
    execute(serialNumber:string,
            accessToken:string):Promise<AssetSynopsisView> {

        return this._httpClient
            .createRequest(`assets`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({serialNumber})
            .send()
            .then(response => {
                return response;
            })
            .then (response => (response.content))
            .then(content => {
                    if (content) {
                        return new AssetSynopsisView(
                            content.id,
                            content.productLineId,
                            content.serialNumber,
                            content.description
                        )
                    }
                }
            );
    }
}

export default SearchForAssetWithSerialNumberFeature;